require "rails_helper"

RSpec.describe Api::V1::ProductsController, type: :controller do
  # Partner-X ==> only access to Product APIs
  # Partner-B ==> have access to All APIs
  # Partner-C ==> not have access to All APIs
  let(:partner_x) { PartnerUser.find_by(fullname: 'Partner-X') }
  let(:partner_b) { PartnerUser.find_by(fullname: 'Partner-B') }
  let(:partner_c) { PartnerUser.find_by(fullname: 'Partner-C') }

  let(:application_json) { 'application/json' }
  let(:success) { 'success' }
  let(:invalid_auth_token) { 'Invalid AuthToken' }
  let(:not_have_permission) { 'Not have permission' }

  describe 'GET lists' do
    it 'Authenticate and Authorize return 200' do
      request.headers['Authorization'] = "Bearer #{partner_x.auth_token}"
      request.headers['UID'] = "#{partner_x.uid}"

      get :lists

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(200)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['status']).to eq(success)
      expect(parsed_body['data']).to eq(
        {
          'products'=> [
            {
              'id' => 1,
              'title' => 'product 1',
              'picture_tumb' => 'https://picsum.photos/id/1/200/300',
              'price' => '$40'
            },
            {
              'id' => 2,
              'title' => 'product 2',
              'picture_tumb' => 'https://picsum.photos/id/2/200/300',
              'price' => '$50'
            }
          ]
        }
      )
    end

    it 'not Authenticate (invalid Auth Token) return 401' do
      request.headers['Authorization'] = "Bearer #{partner_x.auth_token}"
      request.headers['UID'] = "#{partner_b.uid}"

      get :lists

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(401)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['message']).to eq(invalid_auth_token)
    end

    it 'Authenticate but not Authorize' do
      request.headers['Authorization'] = "Bearer #{partner_c.auth_token}"
      request.headers['UID'] = "#{partner_c.uid}"

      get :lists

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(401)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['message']).to eq(not_have_permission)
    end
  end

  describe 'GET detail' do
    let(:product) { Product.find_by(title: 'product 1') }

    it 'Authenticate and Authorize return 200' do
      request.headers['Authorization'] = "Bearer #{partner_x.auth_token}"
      request.headers['UID'] = "#{partner_x.uid}"

      get :detail, params: { id: product.id.to_s }

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(200)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['status']).to eq(success)
      expect(parsed_body['data']).to eq(
        {
          'product'=>
            {
              'id' => 1,
              'title' => 'product 1',
              'picture_tumb' => 'https://picsum.photos/id/1/200/300',
              'price' => '$40',
              'description' => 'desc product 1'
            }
        }
      )
    end

    it 'not Authenticate (invalid Auth Token) return 401' do
      request.headers['Authorization'] = "Bearer #{partner_x.auth_token}"
      request.headers['UID'] = "#{partner_b.uid}"

      get :detail, params: { id: product.id.to_s }

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(401)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['message']).to eq(invalid_auth_token)
    end

    it 'Authenticate but not Authorize' do
      request.headers['Authorization'] = "Bearer #{partner_c.auth_token}"
      request.headers['UID'] = "#{partner_c.uid}"

      get :detail, params: { id: product.id.to_s }
      get :lists

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(401)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['message']).to eq(not_have_permission)
    end
  end
end