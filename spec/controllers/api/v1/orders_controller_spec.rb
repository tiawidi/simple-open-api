require "rails_helper"

RSpec.describe Api::V1::OrdersController, type: :controller do
  # Partner-X ==> only access to Product APIs
  # Partner-B ==> have access to All APIs
  # Partner-C ==> not have access to All APIs
  let(:partner_x) { PartnerUser.find_by(fullname: 'Partner-X') }
  let(:partner_b) { PartnerUser.find_by(fullname: 'Partner-B') }
  let(:partner_c) { PartnerUser.find_by(fullname: 'Partner-C') }

  let(:application_json) { 'application/json' }
  let(:success) { 'success' }
  let(:error) { 'error' }
  let(:invalid_auth_token) { 'Invalid AuthToken' }
  let(:not_have_permission) { 'Not have permission' }

  let(:product) { Product.find_by(title: 'product 1') }

  describe 'POST Add to Cart' do
    it 'Authenticate and Authorize return 200' do
      request.headers['Authorization'] = "Bearer #{partner_b.auth_token}"
      request.headers['UID'] = "#{partner_b.uid}"

      post :add_to_cart, params: { user_id: partner_b.id.to_s, product_id: product.id.to_s }

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(200)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['status']).to eq(success)
      expect(parsed_body['data']).to eq(
        {
          'message' => 'Successfully add a product to cart.'
        }
      )
    end

    it 'not Authenticate (invalid Auth Token) return 401' do
      request.headers['Authorization'] = "Bearer #{partner_x.auth_token}"
      request.headers['UID'] = "#{partner_b.uid}"

      post :add_to_cart, params: { user_id: partner_b.id.to_s, product_id: product.id.to_s }

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(401)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['message']).to eq(invalid_auth_token)
    end

    it 'Authenticate but not Authorize' do
      request.headers['Authorization'] = "Bearer #{partner_x.auth_token}"
      request.headers['UID'] = "#{partner_x.uid}"

      post :add_to_cart, params: { user_id: partner_b.id.to_s, product_id: product.id.to_s }

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(401)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['message']).to eq(not_have_permission)
    end
  end

  describe 'GET Cart Detail' do
    before(:each) do
      cart = Cart.new(customer: partner_b)
      cart.cart_items.build(product_id: product.id, quantity: 1)
      cart.save
    end

    it 'Authenticate and Authorize return 200 (valid params)' do
      request.headers['Authorization'] = "Bearer #{partner_b.auth_token}"
      request.headers['UID'] = "#{partner_b.uid}"

      get :cart_detail, params: { user_id: partner_b.id.to_s }

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(200)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['status']).to eq(success)
      expect(parsed_body['data']).to eq(
        {
          "products" => [
            {
              'id' => 1,
              'tittle' => 'product 1',
              'picture_tumb' => 'https://picsum.photos/id/1/200/300',
              'price' => '$40',
              'qty' => 1
            }
          ]
        }
      )
    end

    it 'Authenticate and Authorize return 200 (invalid params)' do
      request.headers['Authorization'] = "Bearer #{partner_b.auth_token}"
      request.headers['UID'] = "#{partner_b.uid}"

      get :cart_detail, params: { user_id: partner_x.id.to_s }

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(200)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['status']).to eq(error)
      expect(parsed_body['data']).to eq(
        {
          'message' => 'Not allowed'
        }
      )
    end

    it 'not Authenticate (invalid Auth Token) return 401' do
      request.headers['Authorization'] = "Bearer #{partner_x.auth_token}"
      request.headers['UID'] = "#{partner_b.uid}"

      get :cart_detail, params: { user_id: partner_b.id.to_s }

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(401)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['message']).to eq(invalid_auth_token)
    end

    it 'Authenticate but not Authorize' do
      request.headers['Authorization'] = "Bearer #{partner_x.auth_token}"
      request.headers['UID'] = "#{partner_x.uid}"

      get :cart_detail, params: { user_id: partner_b.id.to_s }

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(401)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['message']).to eq(not_have_permission)
    end
  end

  describe 'POST Checkout' do
    before(:each) do
      cart = Cart.new(customer: partner_b)
      cart.cart_items.build(product_id: product.id, quantity: 1)
      cart.save
    end

    it 'Authenticate and Authorize return 200 (valid params)' do
      request.headers['Authorization'] = "Bearer #{partner_b.auth_token}"
      request.headers['UID'] = "#{partner_b.uid}"

      get :checkout, params: { user_id: partner_b.id.to_s }

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(200)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['status']).to eq(success)
      expect(parsed_body['data']).to eq(
        {
          'message' => 'Successfully checkout.'
        }
      )
    end

    it 'Authenticate and Authorize return 200 (invalid params)' do
      request.headers['Authorization'] = "Bearer #{partner_b.auth_token}"
      request.headers['UID'] = "#{partner_b.uid}"

      get :checkout, params: { user_id: partner_x.id.to_s }

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(200)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['status']).to eq(error)
      expect(parsed_body['data']).to eq(
        {
          'message' => 'Not allowed'
        }
      )
    end

    it 'Authenticate and Authorize return 200 (empty cart)' do
      Cart.delete_all

      request.headers['Authorization'] = "Bearer #{partner_b.auth_token}"
      request.headers['UID'] = "#{partner_b.uid}"

      get :checkout, params: { user_id: partner_b.id.to_s }

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(200)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['status']).to eq(error)
      expect(parsed_body['data']).to eq(
        {
          'message' => 'Cart is empty'
        }
      )
    end

    it 'not Authenticate (invalid Auth Token) return 401' do
      request.headers['Authorization'] = "Bearer #{partner_x.auth_token}"
      request.headers['UID'] = "#{partner_b.uid}"

      get :checkout, params: { user_id: partner_b.id.to_s }

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(401)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['message']).to eq(invalid_auth_token)
    end

    it 'Authenticate but not Authorize' do
      request.headers['Authorization'] = "Bearer #{partner_x.auth_token}"
      request.headers['UID'] = "#{partner_x.uid}"

      post :checkout, params: { user_id: partner_b.id.to_s }

      parsed_body = JSON.parse(response.body)

      expect(response.status).to eq(401)
      expect(response.content_type).to eq(application_json)
      expect(parsed_body['message']).to eq(not_have_permission)
    end
  end

end