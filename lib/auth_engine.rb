require 'jwt'

class AuthEngine
    ALGORITHM = 'HS256'
    AUTH_SECRET = "Z\\\ag*N\xFA\xF6\t\xE6-\x92\x1A`\xBF|\xC0\xE9r*\x8B\x96\bR\riE \xC0\xC4\x14\a"

    def self.encode(payload)
      # payload format ==> { uid: 'xxx', token: 'xxx', klass: 'PartnerUser'}

      if payload.present? && payload.is_a?(Hash)
        payload[:token] = Devise.friendly_token
        JWT.encode(payload, AUTH_SECRET, ALGORITHM)
      else
        nil
      end
    end

    def self.decode(token)
        JWT.decode(token, AUTH_SECRET, true, { algorithm: ALGORITHM })
           .try(:first)
    rescue
        nil
    end
end