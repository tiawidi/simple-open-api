Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'pages#home'

  devise_for :users

  devise_scope :user do
      match "/backoffice"  => "devise/sessions#new", via: [:get], as: :unauthenticated_root
      match "signout" => "devise/sessions#destroy", via: [:delete]
  end

  namespace :backoffice do
    resources :partner_users, except: [:destroy] do
      member do
        get   '/add_permission', to: 'partner_users#add_permission', as: 'add_permission'
        post  '/save_permission', to: 'partner_users#save_permission', as: 'save_permission'
        delete '/permission/:user_permit_id', to: 'partner_users#delete_permission', as: 'delete_permission'
      end
    end
    resources :products, except: [:destroy]
    resources :orders, only: [:index, :show]
    resources :carts, only: [:index, :show]
  end

  namespace :api do
    namespace :v1 do
      # Product
      get 'lists', to: 'products#lists', as: 'list_of_products'
      get 'detail/:id', to: 'products#detail', as: 'detail_of_product'

      # Order
      post 'add_to_cart', to: 'orders#add_to_cart', as: 'add_to_cart_order'
      get  'cart/detail/:user_id', to: 'orders#cart_detail', as: 'cart_detail'
      post 'cart/checkout', to: 'orders#checkout', as: 'checkout'
    end
  end
end
