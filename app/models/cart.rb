class Cart < ApplicationRecord

    belongs_to :customer, polymorphic: true
    has_many :cart_items, inverse_of: :cart, dependent: :destroy

end
