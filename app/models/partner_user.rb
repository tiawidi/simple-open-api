class PartnerUser < ApplicationRecord

  before_create :set_uid_and_auth_token

  has_many :user_permits, as: :source
  belongs_to :cart, required: false
  belongs_to :order, required: false

  private

  def set_uid_and_auth_token
    self.uid = SecureRandom.uuid unless self.uid.present?
    self.auth_token = AuthEngine.encode({uid: self.uid, klass: self.class.to_s}) unless self.auth_token.present?
  end
end
