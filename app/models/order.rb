class Order < ApplicationRecord

  belongs_to :customer, polymorphic: true
  has_many :order_items, inverse_of: :order, dependent: :destroy

end
