class UserPermit < ApplicationRecord

  belongs_to :permission
  belongs_to :source, polymorphic: true

end
