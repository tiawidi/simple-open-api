class PagesController < ApplicationController
  before_action :authenticate_user!

  def home
    redirect_to new_user_session_path
  end

end
