module Api
  module V1
    class ProductsController < Api::V1::BaseController

      before_action :authenticate_user, :authorize_request

      def initialize
        super
        @product_service = ::V1::ProductService.new
      end

      def lists
        render_json_result @product_service.lists
      end

      def detail
        if detail_params[:id].present?
          render_json_result @product_service.detail(detail_params[:id])
        else
          render_json_bad_request
        end
      end

      private

      def authorize_request
        authorize_request_for Product
      end

      def detail_params
        params.permit(:id)
      end

    end
  end
end