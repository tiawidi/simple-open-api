module Api
  module V1
    class BaseController < ActionController::API

      def authenticate_user
        header_uid = request.headers['UID']
        header_token = request.headers['Authorization']
        header_token = header_token.split(' ').last if header_token
        if header_uid.present? && header_token.present?
          auth_payload = AuthEngine.decode(header_token)
          if auth_payload['uid'] == header_uid
            @current_user = (auth_payload['klass'].classify.constantize.find_by(uid: auth_payload['uid']) rescue nil)
          end
        end

        render_json_unauthorized 'Invalid AuthToken' unless @current_user.present?
      end

      def authorize_request_for(klass)
        permission = nil

        if current_user.present?
          klass = klass.to_s.downcase

          permissions_id = current_user.user_permits.pluck(:permission_id)
          user_permissions = Permission.where(id: permissions_id)
                                       .as_json(only: [:klass, :action])
                                       .map{|x| x['klass'].present? ? "#{x['klass']}-#{x['action']}" : x['action'] }

          permission = user_permissions & ['manage-all', "#{klass}-manage", "#{klass}-#{self.action_name}"]
        end

        render_json_unauthorized 'Not have permission' unless permission.present?
      end

      def render_json_unauthorized(message=nil)
        message = message || 'Unauthorized Request'
        render json: { message: message }, status: :unauthorized
      end

      def render_json_result(result)
        if result[:status] == :success
          render_json_success result[:data]
        else
          render_json_error result[:data]
        end
      end

      def render_json_success(data)
        render json: { status: 'success', data: data }
      end

      def render_json_error(data)
        render json: { status: 'error', data: data }
      end

      def render_json_bad_request(message=nil)
        message = message || 'Invalid Parameters'
        render json: { errors: message }, status: :bad_request
      end

    end
  end
end
