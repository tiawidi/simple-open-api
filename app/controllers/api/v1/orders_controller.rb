module Api
  module V1
    class OrdersController < Api::V1::BaseController

      before_action :authenticate_user, :authorize_request

      def initialize
        super
        @order_service = ::V1::OrderService.new
      end

      def add_to_cart
        if add_to_cart_params[:user_id].present? && add_to_cart_params[:product_id].present?
          render_json_result @order_service.add_to_cart(add_to_cart_params[:user_id], add_to_cart_params[:product_id])
        else
          render_json_bad_request
        end
      end

      def cart_detail
        if cart_detail_params[:user_id]
          render_json_result @order_service.cart_detail(current_user.id.to_s, cart_detail_params[:user_id])
        else
          render_json_bad_request
        end
      end

      def checkout
        if checkout_params[:user_id].present?
          render_json_result @order_service.checkout(current_user.id.to_s, checkout_params[:user_id])
        else
          render_json_bad_request
        end
      end

      private

      def authorize_request
        authorize_request_for 'Order'
      end

      def add_to_cart_params
        params.permit(:user_id, :product_id)
      end

      def cart_detail_params
        params.permit(:user_id)
      end

      def checkout_params
        params.permit(:user_id)
      end
    end
  end
end
