module Backoffice
  class ProductsController < ApplicationController
    before_action :authenticate_user!

    def index
      @products = Product.all.order(updated_at: :desc)
    end

    def show
      @product = Product.find(params[:id])
    end

    def new
      @product = Product.new
      @url = backoffice_products_path(@product)
    end

    def create
      @product = Product.new(create_params)
      if @product.save
          redirect_to backoffice_product_path(@product), notice: 'Product was successfully created.'
      else
        @url = backoffice_products_path(@product)
        flash[:alert] = @product.errors.full_messages
        render :new
      end
    end

    def edit
      @product = Product.find(params[:id])
      @url = backoffice_product_path(@product)
    end

    def update
      @product = Product.find(params[:id])
      if @product.update(update_params)
        redirect_to backoffice_product_path(@product), notice: 'Product was successfully updated.'
      else
        @url = backoffice_product_path(@product)
        flash[:alert] = @product.errors.full_messages
        render :edit
      end
    end

    private

    def create_params
      params.require(:product).permit(:title, :price, :picture_tumb, :description)
    end

    def update_params
      params.require(:product).permit(:title, :price, :picture_tumb, :description)
    end
  end
end
