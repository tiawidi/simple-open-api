module Backoffice
  class CartsController < ApplicationController
    before_action :authenticate_user!

    def index
      @carts = Cart.all
    end

    def show
      @cart = Cart.find(params[:id])
    end
  end
end
