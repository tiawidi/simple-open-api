module Backoffice
  class PartnerUsersController < ApplicationController

    before_action :authenticate_user!

    def index
      @partner_users = PartnerUser.all
    end

    def show
      @partner_user = PartnerUser.find(params[:id])
    end

    def new
      @partner_user = PartnerUser.new
      @url = backoffice_partner_users_path(@partner_user)
    end

    def create
      @partner_user = PartnerUser.new(create_params)
      if @partner_user.save
        redirect_to backoffice_partner_user_path(@partner_user), notice: 'Partner User was successfully created.'
      else
        @url = backoffice_partner_users_path(@partner_user)
        flash[:alert] = @partner_user.errors.full_messages
        render :new
      end
    end

    def edit
      @partner_user = PartnerUser.find(params[:id])
      @url = backoffice_partner_user_path(@partner_user)
    end

    def update
      @partner_user = PartnerUser.find(params[:id])
      if @partner_user.update(update_params)
        redirect_to backoffice_partner_user_path(@partner_user), notice: 'Partner User was successfully updated.'
      else
        @url = backoffice_partner_user_path(@partner_user)
        flash[:alert] = @partner_user.errors.full_messages
        render :edit
      end
    end

    def add_permission
      @partner_user = PartnerUser.find(params[:id])
      @permission_options = Permission.all.collect{ |p| [p.name, p.id]}
      @url = save_permission_backoffice_partner_user_path(@partner_user)
    end

    def save_permission
      @partner_user = PartnerUser.find(params[:id])
      permission = Permission.find(params[:permission_id])

      if permission.present?
        user_permit = UserPermit.find_by(source: @partner_user, permission: permission)
        if user_permit.present?
          redirect_to backoffice_partner_user_path(@partner_user), notice: "Partner User was have assigned APIs Permission for #{permission.name} before."
        else
          user_permit = UserPermit.new(source: @partner_user, permission: permission)
          if user_permit.save
            redirect_to backoffice_partner_user_path(@partner_user), notice: 'Partner User was successfully added new APIs Permission.'
          else
            @url = save_permission_backoffice_partner_user_path(@partner_user)
            flash[:alert] = user_permit.errors.full_messages
            render :add_permission
          end
        end
      else
        @url = save_permission_backoffice_partner_user_path(@partner_user)
        flash[:alert] = @partner_user.errors.full_messages
        render :add_permission
      end
    end

    def delete_permission
      @partner_user = PartnerUser.find(params[:id])
      user_permit = UserPermit.find(params[:user_permit_id])
      if user_permit.present? && user_permit.source == @partner_user
        if user_permit.delete
          redirect_to backoffice_partner_user_path(@partner_user), notice: 'Successfully to delete APIs Permission from Partner User.'
        else
          redirect_to backoffice_partner_user_path(@partner_user), alert: 'Failed to delete APIs Permission from Partner User.'
        end
      else
        redirect_to backoffice_partner_user_path(@partner_user), alert: 'Failed to delete APIs Permission from Partner User.'
      end
    end

    private

    def create_params
      params.require(:partner_user).permit(:fullname, :phone, :address)
    end

    def update_params
      params.require(:partner_user).permit(:fullname, :phone, :address)
    end

  end
end
