module V1
  class ProductService < V1::BaseService

    def lists
      data = {
        products: (Product.all.as_json(only: [:id, :title, :picture_tumb, :price]) rescue [] )
      }

      success_result data
    end

    def detail(id)
      product = Product.find_by(id: id)
      if product.present?
        data = {
          product: product.as_json(only: [:id, :title, :picture_tumb, :price, :description])
        }

        success_result data
      else
        failed_result({ message: 'Product is not found.'})
      end
    end
  end
end