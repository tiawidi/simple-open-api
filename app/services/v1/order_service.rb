module V1
  class OrderService < V1::BaseService

    def add_to_cart(partner_user_id, product_id)
      partner_user = PartnerUser.find_by(id: partner_user_id)
      product = Product.find_by(id: product_id)

      if partner_user.present? && product.present?
        cart = Cart.find_by(customer: partner_user)
        if cart.present?
          cart_item = cart.cart_items.find_by(product_id: product.id)
          if cart_item.present?
            cart_item.quantity += 1
            if cart_item.save
              return success_result({ message: 'Successfully update a product in cart.'})
            else
              return failed_result({ message: cart_item.errors.full_messages.to_sentence})
            end
          else
            cart.cart_items.build(product_id: product.id, quantity: 1)
          end
        else
          cart = Cart.new(customer: partner_user)
          cart.cart_items.build(product_id: product.id, quantity: 1)
        end

        if cart.save
          return success_result({ message: 'Successfully add a product to cart.'})
        else
          return failed_result({ message: cart.errors.full_messages.to_sentence})
        end
      else
        messages = []
        messages << 'Partner User is not found' if !partner_user.present?
        messages << 'Product is not found' if !partner_user.present?

        failed_result({ message: messages.to_sentence })
      end
    end

    def cart_detail(current_user_id, partner_user_id)
      return failed_result({ message: 'Not allowed' }) if current_user_id != partner_user_id

      partner_user = PartnerUser.find_by(id: partner_user_id)
      if partner_user.present?
        data = { products: [] }

        cart = Cart.find_by(customer: partner_user)
        if cart.present?
          cart.cart_items.each do |cart_item|
            product = cart_item.product
            data[:products] << {
              id: cart_item.product_id,
              tittle: product.title,
              picture_tumb: product.picture_tumb,
              price: product.price,
              qty: cart_item.quantity
            }
          end
        end

        success_result(data)
      else
        failed_result({ message: 'Partner User is not found' })
      end
    end

    def checkout(current_user_id, partner_user_id)
      return failed_result({ message: 'Not allowed' }) if current_user_id != partner_user_id

      partner_user = PartnerUser.find_by(id: partner_user_id)
      if partner_user.present?
        cart = Cart.find_by(customer: partner_user)
        if cart.present?
          customer = cart.customer
          order = Order.new(
            customer: customer,
            fullname: customer.fullname,
            phone: customer.phone,
            address: customer.address
          )

          cart.cart_items.each do |cart_item|
            order.order_items.build(
              product_id: cart_item.product_id,
              quantity: cart_item.quantity
            )
          end

          if order.save
            cart.delete
            success_result({ message: 'Successfully checkout.'})
          else
            failed_result({ message: order.errors.full_messages.to_sentence})
          end
        else
          failed_result({ message: 'Cart is empty'})
        end
      else
        failed_result({ message: 'Partner User is not found' })
      end
    end

  end
end
