module V1
  class BaseService

    def success_result(data)
      {
        status: :success,
        data: data
      }
    end

    def failed_result(data)
      {
        status: :failed,
        data: data
      }
    end
  end
end
