# README

## Overview

This project explain how to use Authorization and Authentication for APIs, is user have authenticate and authorize for APIs request.

### Use Case
![Use-Case!](/app/assets/images/use-case.png)


![Swinline!](/app/assets/images/swimline.png)


### Database Schema
![Scheema!](/app/assets/images/simple-open-api-scheema.png)


## How to Run?
1. Clone the repo over SSH or HTTPS
    ```term
    git clone git@bitbucket.org:tiawidi/simple-open-api.git
    ```
    ```term
    git clone https://tiawidi@bitbucket.org/tiawidi/simple-open-api.git
    ```
2. Install rvm
    * RVM package for Ubuntu
    [RVM package for Ubuntu](https://github.com/rvm/ubuntu_rvm)

3. Install ruby version 2.6.8
    ```term
    rvm install 2.1.1
    ```

4. Run bundle install
    ```term
    bundle install
    ```
5. Migrate Database
    ```term
    rake db:prepare
    ```
6. Run Rails Server
    ```
    rails s
    ```

### How to Add User and assigne or remove APIs Permission?
1. Run Rails Server

    ```
    rails s
    ```

2. Open Backoffice from Browser, in login form fill email and password with:
    ```
    email => admin@simple-open-api
    password => password
    ```
    ![Login-Page!](/app/assets/images/login-page.png)

    As a default after login will be direct to  List of Partner User Page
    ![Login-Page!](/app/assets/images/list-of-partner-user-page.png)

3. For assigne or remove APIs Permission, open Partner User Detail Page with click show link
    ![Login-Page!](/app/assets/images/partner-user-detail-page.png)

    ![Login-Page!](/app/assets/images/assigne-api-permission-page.png)


## Unit Test
Used RSpec for unit test.

### Step to run unit test:

1. Migrate database with test environment
    ```term
    rake db:prepare RAILS_ENV=test
    ```
2. Run unit test

    * Run all unit test
        ```term
        rspec spec/controllers/api/v1/
        ```
        ![Use-Case!](/app/assets/images/all_spec_result.png)


    * Run specific unit test
        ```term
        rspec spec/controllers/api/v1/products_controller_spec.rb
        ```
        ![Use-Case!](/app/assets/images/orders_controller_spec_result.png)


        ```term
        rspec spec/controllers/api/v1/orders_controller_spec.rb
        ```
        ![Use-Case!](/app/assets/images/products_controller_spec_result.png)


## Documentations
* [API Documentation](https://documenter.getpostman.com/view/5627262/UVsFyo2f)
* [Postman Collection](https://www.getpostman.com/collections/38b08c9f9ca8b3333d0a)