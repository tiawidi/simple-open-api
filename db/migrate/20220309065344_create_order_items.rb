class CreateOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_items do |t|
      t.integer :quantity

      t.timestamps
    end

    add_column :order_items, :order_id, :integer
    add_column :order_items, :product_id, :integer
    
    add_foreign_key :order_items, :carts, column: :order_id
    add_foreign_key :order_items, :products, column: :product_id

    add_index :order_items, :order_id
    add_index :order_items, :product_id
  end
end
