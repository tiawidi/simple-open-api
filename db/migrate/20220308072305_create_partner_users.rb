class CreatePartnerUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :partner_users do |t|
      t.string :fullname
      t.string :phone
      t.string :address
      t.string :uid
      t.string :auth_token

      t.timestamps
    end
  end
end
