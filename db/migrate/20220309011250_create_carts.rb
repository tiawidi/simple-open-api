class CreateCarts < ActiveRecord::Migration[5.2]
  def change
    create_table :carts do |t|

      t.timestamps
    end

    add_column :carts, :customer_id, :integer
    add_column :carts, :customer_type, :string

    add_index :carts, [:customer_id, :customer_type]
  end
end
