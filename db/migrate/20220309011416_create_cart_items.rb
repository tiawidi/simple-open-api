class CreateCartItems < ActiveRecord::Migration[5.2]
  def change
    create_table :cart_items do |t|
      t.integer :quantity

      t.timestamps
    end

    add_column :cart_items, :cart_id, :integer
    add_column :cart_items, :product_id, :integer
    
    add_foreign_key :cart_items, :carts, column: :cart_id
    add_foreign_key :cart_items, :products, column: :product_id

    add_index :cart_items, :cart_id
    add_index :cart_items, :product_id
  end
end
