class CreatePermissions < ActiveRecord::Migration[5.2]
  def change
    create_table :permissions do |t|
      t.string :code
      t.string :name
      t.string :description
      t.string :klass
      t.string :action

      t.timestamps
    end
  end
end
