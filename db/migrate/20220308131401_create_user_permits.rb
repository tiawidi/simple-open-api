class CreateUserPermits < ActiveRecord::Migration[5.2]
  def change
    create_table :user_permits do |t|

      t.timestamps
    end

    add_column :user_permits, :permission_id, :integer
    add_column :user_permits, :source_id, :integer
    add_column :user_permits, :source_type, :string

    add_foreign_key :user_permits, :permissions, column: :permission_id

    add_index :user_permits, :permission_id
    add_index :user_permits, [:source_id, :source_type]
  end
end
