class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :fullname
      t.string :phone
      t.string :address

      t.timestamps
    end

    add_column :orders, :customer_id, :integer
    add_column :orders, :customer_type, :string

    add_index :orders, [:customer_id, :customer_type]
  end
end
