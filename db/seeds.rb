# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
puts "Generating Admin User [START]"
  user = User.new(email: "admin@simple-open-api", password: 'password', password_confirmation: 'password')
  user.save!
puts "Generating Admin User [END]"

puts "Generating Products Data [START]"
  [
    {
      id: 1,
      title: "product 1",
      picture_tumb: "https://picsum.photos/id/1/200/300",
      price: "$40",
      description: "desc product 1"
    },
    {
      id: 2,
      title: "product 2",
      picture_tumb: "https://picsum.photos/id/2/200/300",
      price: "$50",
      description: "desc product 2"
    }
  ].each do |data|
    Product.find_or_create_by(title: data[:title]) do |p|
      p.id = data[:id]
      p.picture_tumb = data[:picture_tumb]
      p.price = data[:price]
      p.description = data[:description]
    end
  end
puts "Generating Products Data [DONE]"

puts "Generating Partner Users Data [START]"
  [
    {
      uid: '2f9b0413-af82-48f7-9143-b54992c06912',
      fullname: 'Partner-X',
      phone: "081200000000",
      address: "Jakarta"
    },
    {
      uid: 'a7cdadd5-5452-4815-b09b-d5b98e40d8db',
      fullname: 'Partner-B',
      phone: "081211111111",
      address: "Jawa Barat"
    },
    {
      uid: '2786fef0-52e4-4b5a-b3fc-035be3a27189',
      fullname: 'Partner-C',
      phone: "081222222222",
      address: "Bandung"
    }
  ].each do |data|
    PartnerUser.find_or_create_by(uid: data[:uid]) do |p|
      p.fullname = data[:fullname]
      p.phone = data[:phone]
      p.address = data[:address]
      p.auth_token = AuthEngine.encode({uid: data[:uid], klass: PartnerUser.to_s})
    end
  end
puts "Generating Partner Users Data [END]"

puts "Generating Permissions Data [START]"
  permissions_data = {
    'all': [
      {
        name: 'All operations',
        description: 'Manage Everything',
        klass: nil,
        action: 'manage-all'
      }
    ],
    'product': [
      {
        name: 'Manage Product',
        description: '',
        klass: 'product',
        action: 'manage'
      },
      {
        name: 'List of Products',
        description: '',
        klass: 'product',
        action: 'lists'
      },
      {
        name: 'Detail of Product',
        description: '',
        klass: 'product',
        action: 'detail'
      }
    ],
    'order': [
      {
        name: 'Manage Order',
        description: '',
        klass: 'order',
        action: 'manage'
      },
      {
        name: 'Add to Cart',
        description: '',
        klass: 'order',
        action: 'add_to_cart'
      },
      {
        name: 'Cart Detail',
        description: '',
        klass: 'order',
        action: 'cart_detail'
      },
      {
        name: 'Checkout Order',
        description: '',
        klass: 'order',
        action: 'checkout'
      },
    ]
  }

  permission_counter = 0
  permissions_data.each do |key, values|
    values.each do |value|
      Permission.find_or_create_by(klass: value[:klass], action: value[:action]) do |p|
        p.code = "PERM#{permission_counter}"
        p.name = value[:name]
        p.description = value[:description]
      end
      permission_counter += 1
    end
  end
puts "Generating Permissions Data [END]"

puts "Generating Permissions for Users Data [START]"
  [
    {
      fullname: 'Partner-X',
      permissions: [
        { klass: 'product', action: 'manage' }
      ]
    },
    {
      fullname: 'Partner-B',
      permissions: [
        { klass: nil, action: 'manage-all' }
      ]
    }
  ].each do |data|
    p_user = PartnerUser.find_by(fullname: data[:fullname])
    data[:permissions].each do |permission|
      permission = Permission.find_by(klass: permission[:klass], action: permission[:action])
      UserPermit.find_or_create_by(source: p_user, permission_id: permission.id)
    end
  end
puts "Generating Permissions for Users Data [END]"
